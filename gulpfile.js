'use strict';

// Init Gulp plugins
const
    gulp       = require('gulp'),
    bs         = require('browser-sync'), // browser-sync with live-reload
    reload     = bs.reload,

    sourcemaps = require('gulp-sourcemaps'), // improve debug process

    babel      = require('gulp-babel'), // https://babeljs.io/
    include    = require('gulp-include'), // https://www.npmjs.com/package/gulp-include

    nunjucks   = require('gulp-nunjucks-render'), // docs: http://jinja.pocoo.org/docs/2.10/

    sass       = require('gulp-sass'), // https://sass-lang.com/
    postcss    = require('gulp-postcss'),
    prefixer   = require('autoprefixer'),
    cssnano    = require('cssnano'),

    wait       = require('gulp-wait'),
    pump       = require('pump'),
    notify     = require('gulp-notify'),
    mv         = require('gulp-replace-name'),
    rimraf     = require('rimraf')

require('events').EventEmitter.defaultMaxListeners = Infinity

// Config
let srv = {
        server         : './app/',
        host           : 'localhost',
        port           : 5001,
        notify         : true,
        tunnel         : false,
        open           : false,
    },
    path = {
        src: {
            scss      : './src/scss/*.scss',
            js        : './src/js/*.bundle.js',
            pages     : './src/html/pages/*.+(html|nunjucks)',
            templates : './src/html/templates/'
        },
        app: {
            root : './app/',
            js   : './app/js/',
            css  : './app/css/',
        },
        watch: {
            scss     : './src/scss/**/*.scss',
            js       : './src/js/**/*.js',
            html     : 'src/html/**/*.+(html|nunjucks)',
        },
        clean  : "./app/",
    }

// Convert ES6→ES5, concat, minify & uglify JavaScript files
function buildScripts(cb) {
    pump([
            gulp.src(path.src.js),
            sourcemaps.init({
                loadMaps: true
            }),
            include(),
            babel({
                presets: ['@babel/env']
            }),
            sourcemaps.write('./maps', {
                includeContent: false
            }),
            mv(/\.bundle/g, '.min'),
            gulp.dest(path.app.js),
            wait(500),
            reload({ stream: true }),
        ],
        cb)
}

// Build, minify & concat SCSS
function buildStyles(cb) {
    let plugins = [
        prefixer(),
        cssnano()
    ]
    gulp.src(path.src.scss)
        .pipe(sourcemaps.init({ loadMaps: true }))
        .pipe(sass({
            sourceMap: true,
            errLogConsole: true
        }))
        .on("error", notify.onError({
            message: "Error: <%= error.message %>",
            title: "SASS Error"
        }))
        .pipe(postcss(plugins))
        .pipe(
            sourcemaps.write('maps', {
                includeContent: false,
                sourceRoot: 'source'
            })
        )
        .pipe(gulp.dest(path.app.css))
        .pipe(bs.stream({ match: '**/*.css' }))
    cb()
}

// Render template with Nunjucks
function renderTemplates(cb) {
    console.info('Nunjucks: templates rendered');
    pump([
            gulp.src(path.src.pages),
            nunjucks({
                path: [path.src.templates],
            }),
            gulp.dest(path.app.root),
            reload({ stream: true }),
        ],
    cb)
}

// Monitoring files changes
function watch() {
    gulp.watch([path.watch.js], buildScripts)
    gulp.watch([path.watch.scss], buildStyles)
    gulp.watch([path.watch.html], renderTemplates)
}

// Run browser-sync webserver
function webserver() {
    return bs(srv)
}

// Clean previous build
function clean(cb) {
    console.log('Clean previous build with gulp!')
    rimraf(path.clean, cb)
}

// Main task
const main = gulp.series(
    buildScripts, buildStyles, renderTemplates,
    gulp.parallel(watch, webserver)
)

// Exports
exports.scripts   = buildScripts
exports.styles    = buildStyles
exports.templates = renderTemplates
exports.clean     = clean
exports.default   = main